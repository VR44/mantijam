﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadLevel : MonoBehaviour
{
    public Button Level1Btn,Level2Btn, Level3Btn;

    // Start is called before the first frame update
    void Start()
    {
        //Calls the TaskOnClick/TaskWithParameters/ButtonClicked method when you click the Button
        Level1Btn.onClick.AddListener(() => LoadScene("Scenes/Levels/Level1"));
        Level2Btn.onClick.AddListener(() => LoadScene("Scenes/Levels/Level2"));
        Level3Btn.onClick.AddListener(() => LoadScene("Scenes/Levels/Level3"));
    }

    void LoadScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
        Debug.Log("Button clicked = " + SceneName);
    }
    void Update()
    {
        
    }
}
