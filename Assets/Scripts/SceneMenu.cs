﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneMenu : MonoBehaviour
{
    public Button LoadLevelBtn, RestartLevelBtn;
    public LevelSettings LevelSettings;

    void Start()
    {
        RestartLevelBtn.onClick.AddListener(RestartScene);
        LoadLevelBtn.onClick.AddListener(NextScene);
        RestartLevelBtn.gameObject.SetActive(false);
        LoadLevelBtn.gameObject.SetActive(false);
    }

    public void NextScene()
    {
        LoadScene(LevelSettings.NextScene);
    }
    public void ShowNextLevelButton()
    {
        LoadLevelBtn.gameObject.SetActive(true);
    }
    public void ShowRestartButton()
    {
        RestartLevelBtn.gameObject.SetActive(true);
    }
    void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    void LoadScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
    void Update()
    {

    }
}

