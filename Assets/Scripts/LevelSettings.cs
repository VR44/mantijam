﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSettings : MonoBehaviour
{
    public string LevelName = "Level1";
    public string NextScene = "Scenes/Levels/Level2";
    public int Difficulty = 15;
    public float GravityModifier = -150.0F;
    private void Start()
    {
        //Physics.gravity = new Vector3(0, 0, GravityModifier);
    }
}
