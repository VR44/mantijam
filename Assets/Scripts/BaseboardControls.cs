﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BaseboardControls : MonoBehaviour
{
    public Text BounceLabel;
    public int BounceCount = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Vector3 position = this.transform.position;
            position.x += 3;
            this.transform.position = Vector3.SmoothDamp(this.transform.position, position, ref velocity, smoothTime);
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            Vector3 position = this.transform.position;
            position.x -= 3;
            this.transform.position = Vector3.SmoothDamp(this.transform.position, position, ref velocity, smoothTime);
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        var rotationVector = this.transform.rotation.eulerAngles;
        rotationVector.y = newVectorY();
        this.transform.rotation = Quaternion.Euler(rotationVector);

        this.transform.localScale += new Vector3(-0.3F, 0, 0);
        updateBounces();
        
    }

    int newVectorY()
    {
        System.Random random = new System.Random();
        return random.Next(-20, 20);
    }

    void updateBounces()
    {
        BounceLabel.text = "Bounces: " + BounceCount++.ToString();
    }
}