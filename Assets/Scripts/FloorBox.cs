﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloorBox : MonoBehaviour
{
    int WinningCount = 9;
    public BaseboardControls BaseBoard;
    public SceneMenu SceneMenu;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
    }

    void SetWinningCount(int newCount)
    {
        WinningCount = newCount;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    int GetBounceCount()
    {
        return BaseBoard.BounceCount;
    }

    void ShowGameMenu(bool nextLevel)
    {
        SceneMenu.ShowRestartButton();
        if (nextLevel)
        {
            SceneMenu.ShowNextLevelButton();
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        
        if (GetBounceCount() >= WinningCount)
        {
            Debug.Log("next level");
            ShowGameMenu(true);
        }
        else
        {
            ShowGameMenu(false);
        }
        Time.timeScale = 0;
    }
}
